Feature: Add Another Phone to Cart

  Scenario Outline: Add Another Phone to Cart with sufficient stock
    Given I have login in the shopping website
    When I add <amount> phone into the cart
    And I subtract the amount of the phone in the inventory
    Then I can see <amount> phone in my cart
    And The amount of phone in the inventory should be <reminder>
    Examples:
      | amount | reminder |
      | 1      | 99       |
      | 3      | 97       |
      | 4      | 96       |