Feature: Add Another Phone to Cart

  Scenario: Add Another Phone to Cart with sufficient stock
    Given I have login in the shopping website
    When I add one phone into the cart
    And I subtract the amount of the phone in the inventory
    Then I can see one phone in my cart
    And The amount of phone in the inventory should be 99