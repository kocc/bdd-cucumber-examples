package com.course.examples.test.defs;

import com.cucumber.examples.shopping.Person;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class AddPhoneToCartStepParamDefinition {

    private Person person;

    /*@Given("I have login in the shopping website")
    public void login(){

    }*/

    @When("I add {int} phone into the cart")
    public void addGoodsToCart() {

    }

    @Then("I can see {int} phone in my cart")
    public void iCanSee(){
        Assert.assertEquals("I cannot see the phone in my cart","","");
    }

    @And("And The amount of phone in the inventory should be <reminder>")
    public void theInventory(int arg0) {
        Assert.assertEquals("The amount of phone in the inventory is incorrect","","");
    }
    

}
