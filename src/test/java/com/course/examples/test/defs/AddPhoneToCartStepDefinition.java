package com.course.examples.test.defs;

import com.cucumber.examples.context.LoginContext;
import com.cucumber.examples.shopping.Cart;
import com.cucumber.examples.shopping.Goods;
import com.cucumber.examples.shopping.Person;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

public class AddPhoneToCartStepDefinition {

    private Person person;

    @Given("I have login in the shopping website")
    public void login(){
        person = new Person("ray");
        LoginContext.login(person);
    }

    @When("I add one phone into the cart")
    public void addGoodsToCart() {
        Cart.of(person).addGoods(new Goods("huawei"), 2);
    }

    @Then("I can see one phone in my cart")
    public void iCanSee(){
        Assert.assertEquals("I cannot see the phone in my cart",
                2,
                person.getCart().getGoodsAmount(new Goods("huawei")));
    }

    @And("I subtract the amount of the phone in the inventory")
    public void subtract() {
    }

    @And("The amount of phone in the inventory should be {int}")
    public void theInventory(int arg0) {
        Assert.assertEquals("The amount of phone in the inventory is incorrect","","");
    }


}
