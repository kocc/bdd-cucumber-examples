package com.course.examples.test.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(monochrome = true,
        features = {"classpath:features/cart/addPhoneToCart.feature"},
        glue = "com.course.examples.test.defs",
        plugin = "html:target/cucumber"
)
public class AddPhoneToCartTestRunner {



}
