package com.cucumber.examples.shopping;

import com.cucumber.examples.context.LoginContext;
import com.cucumber.examples.service.impl.OrderServiceImpl;
import lombok.Data;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

@ToString
@Data
public class Cart extends BaseModel {

    private Person person;

    private Map<Goods, Integer> goodsMap = new HashMap<>();

    private Cart(Person person) {
        this.person = person;
        this.person.setCart(this);
    }

    public void addGoods(Goods goods, int amount) {
        if (Inventory.stockOf(goods) < 0){
            throw new RuntimeException("Not sufficient goods in inventory");
        }
        if(goodsMap.get(goods) != null){
            goods.put(goods, goodsMap.get(goods) + amount);
        }else {
            this.goodsMap.put(goods,amount);
        }
    }

    public int getGoodsAmount(Goods goods){
        return goodsMap.getOrDefault(goods, 0);
    }

    public void checkout(){
        LoginContext.loginRequired();
        new OrderServiceImpl().generate(this);
    }

    public static Cart of(Person person) {
        LoginContext.loginRequired();
        return new Cart(person);
    }

}
