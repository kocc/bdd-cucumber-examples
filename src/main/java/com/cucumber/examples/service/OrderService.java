package com.cucumber.examples.service;

import com.cucumber.examples.shopping.Cart;
import com.cucumber.examples.shopping.Order;

public interface OrderService {

    Order generate(Cart cart);
}
