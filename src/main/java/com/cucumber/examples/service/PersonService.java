package com.cucumber.examples.service;

import com.cucumber.examples.shopping.Person;

public interface PersonService {

    void login(Person person);
}
