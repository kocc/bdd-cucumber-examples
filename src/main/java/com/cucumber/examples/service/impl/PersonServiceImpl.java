package com.cucumber.examples.service.impl;

import com.cucumber.examples.context.LoginContext;
import com.cucumber.examples.service.PersonService;
import com.cucumber.examples.shopping.Person;


public class PersonServiceImpl implements PersonService {
    @Override
    public void login(Person person) {
        if (!LoginContext.hasLogin()) {
            LoginContext.login(person);
        }
    }
}
