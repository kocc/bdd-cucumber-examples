package com.cucumber.examples.service;


import com.cucumber.examples.shopping.Goods;
import com.cucumber.examples.shopping.Order;

public interface CartService {

    void addGoods(Goods goods, int amount);

    int getGoodsAmount(Goods goods);

    void checkout(Order order);

}
