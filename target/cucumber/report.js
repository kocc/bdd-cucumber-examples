$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("classpath:features/cart/addPhoneToCart.feature");
formatter.feature({
  "name": "Add Another Phone to Cart",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Add Another Phone to Cart with sufficient stock",
  "description": "",
  "keyword": "Scenario"
});
formatter.step({
  "name": "I have login in the shopping website",
  "keyword": "Given "
});
formatter.match({
  "location": "AddPhoneToCartStepDefinition.login()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I add one phone into the cart",
  "keyword": "When "
});
formatter.match({
  "location": "AddPhoneToCartStepDefinition.addGoodsToCart()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I subtract the amount of the phone in the inventory",
  "keyword": "And "
});
formatter.match({
  "location": "AddPhoneToCartStepDefinition.subtract()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I can see one phone in my cart",
  "keyword": "Then "
});
formatter.match({
  "location": "AddPhoneToCartStepDefinition.iCanSee()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "The amount of phone in the inventory should be 99",
  "keyword": "And "
});
formatter.match({
  "location": "AddPhoneToCartStepDefinition.theInventory(int)"
});
formatter.result({
  "status": "passed"
});
});